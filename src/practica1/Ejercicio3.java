package practica1;

/**
 *
 * @author hokage
 */
public class Ejercicio3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // 1. Iniciar array 15 valoraciones
        // Pueden ser del 1 al 15
        int[] valoraciones = {
            4, 5, 4, 5, 5,
            1, 5, 4, 5, 4,
            5, 3, 4, 4, 2
        };
        // 2. Variables para almacenar los resultados.
        double suma = 0, mitjanaVal = 0;

        // Posible solucion int val1=0,val2,...
        // Pero mola más un array.
        int[] numClientsValoracio = new int[5];
        for (int valoracion : valoraciones) {
            // Sumamos 1 punto 
            numClientsValoracio[valoracion - 1]++;
            // C
            suma = suma + valoracion;
        }

        // 4. Hacemos la media 
        // SUMA / TOTAL. pej 60 / 15 = 4.0
        // valoraciones.length => 15 valoraciones
        mitjanaVal = suma
                / (double) valoraciones.length;

        // 5. Resultados: 
        // System.out.println("suma="+suma);
        System.out.println("mitjanaVal=" + mitjanaVal);

        // 6. Media valoraciones.
        int cont = 1;
        for (int valor : numClientsValoracio) {
            System.out.println(
                    "Valoración " + cont + " * = " + valor);
            cont++;
        }
        System.out.println("mitjanaVal=" + mitjanaVal);
    }
}
