package practica1;

/**
 *
 * @author hokage
 */
public class Ejercicio4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String[] ciutats = {"Badalona", "Barcelona", "Constantí", "Granollers",
            "Hospitalet", "Igualada", "Lleida", "Manlleu"};
        // Array's
        int[] dades2017 = {81, 90, 97, 89, 83, 89, 86, 87};
        int[] dades2018 = {30, -117, -80, -101, -102, 27, -50, -10};
        int[] dades2019 = {51, 52, 55, 49, 55, 61, 55, 54};

        System.out.println("\t -------- Ciutats --------");
        for (int i = 0; i < ciutats.length; i++) {
            System.out.println("\t \t "+ciutats[i]);
        }
        
        System.out.println("\n \t -------- Mitjanes --------");
        med2017(dades2017);
        med2018(dades2018);
        med2019(dades2019);

    }

    /**
     * average air 2017
     *
     * @param dad2017
     */
    private static void med2017(int[] dad2017) {

        double media2017 = 0.0;
        for (int i = 0; i < dad2017.length; i++) {
            media2017 = media2017 + dad2017[i];
            media2017 = media2017 / dad2017.length;
        }
        System.out.printf("\t \t 2017: %.2f \n", media2017); 

    }

    /**
     * average air 2018
     *
     * @param dad2018
     */
    private static void med2018(int[] dad2018) {

        double media2018 = 0.0;
        for (int i = 0; i < dad2018.length; i++) {
            media2018 = media2018 + dad2018[i];
            media2018 = media2018 / dad2018.length;
        }
        System.out.printf("\t \t 2018: %.2f \n", media2018); 

    }

    /**
     * Average air 2019
     *
     * @param dad2019
     */
    private static void med2019(int[] dad2019) {

        double media2019 = 0.0;
        for (int i = 0; i < dad2019.length; i++) {
            media2019 = media2019 + dad2019[i];
            media2019 = media2019 / dad2019.length;
        }
        System.out.printf("\t \t 2019: %.2f \n", media2019); 

    }
    
}
