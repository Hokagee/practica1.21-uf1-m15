package practica1;

import java.util.Scanner;

/**
 *
 * @author hokage
 */
public class Ejercicio2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner inputText = new Scanner(System.in);
        String cadena;
        String[] arrayStrings;
        
        System.out.print("Insert your sentence: ");
        cadena = inputText.nextLine();
        
        arrayStrings = cadena.split(" ");
        
        System.out.println("\nLas palabras que tienen mas de 5 caracteres son las siguientes: ");
        for (int i = 0; i < arrayStrings.length; i++) {
            if (arrayStrings[i].length() >= 5) {
                System.out.println(arrayStrings[i]);
            }
        }
        
        
    }
    
}
