package practica1;

import java.util.Scanner;

/**
 *
 * @author hokage
 */
public class Ejercicio1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner inputText = new Scanner(System.in);
        String cadena = "";
        String cadenaP = "";
        
        System.out.print("Insert your word: ");
        cadena = inputText.nextLine();
        
        cadenaP = cadena.replace('P', '?');
        System.out.println("Your word "+cadena+" without P is: "+cadenaP);
    }
    // Te añado un comentario aqui Miquel. Este es el unico cambio
    
}
